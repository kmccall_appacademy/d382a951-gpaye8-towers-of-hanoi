# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers, :turn

  def initialize(tower=[3,2,1])
    @towers = [tower]
    @towers << []
    @towers << []
    @turn = 0
  end

  def move(from, to)
    raise 'Invalid move' unless valid_move?(from, to)
    @towers[to] << @towers[from].pop
  end

  def valid_move?(from, to)
    return false if @towers[from].empty?
    return false unless (@towers[to].empty? || (@towers[from].last < @towers[to].last))
    return true
  end

  def won?
    return false unless @towers[0].empty?
    @towers[1].size == 3 || @towers[2].size == 3
  end

  def render
    t = []
    m = []
    b = []
    @towers.each_index { |i| b << (@towers[i].size > 0 ? [@towers[i][0]] : [0]) }
    @towers.each_index { |i| m << (@towers[i].size > 1 ? [@towers[i][1]] : [0]) }
    @towers.each_index { |i| t << (@towers[i].size > 2 ? [@towers[i][2]] : [0]) }
    print t; print "\n"
    print m; print "\n"
    print b; print "\n"
  end

  def new_turn
    print "\n Turn number #{@turn} \n"
    render
    print "\n Enter your move, e.g 0,1 \n"
    new_move = gets.chomp
    new_move = new_move.split(',').map &:to_i
    move(new_move[0],new_move[1])
    @turn += 1
  end

  def play
    until won?
      new_turn
    end
    print "Yay, you won in #{turn} turns"
  end

end

game = TowersOfHanoi.new
game.play
